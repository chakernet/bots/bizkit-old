/*
	Command Handler Test(s)
    Copyright (C) 2021 Jack C <jack@chaker.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"testing"

	"github.com/diamondburned/arikawa/v3/discord"
)

var (
	prefix = "!"
)

func TestSimpleParse(t *testing.T) {
	name, role := "Jack C", "President"

	handler := CommandHandler {
		Commands: []Command{
			{
				Id: "test",
				Aliases: []string{"test", "test2"},
				Args: []Arg{
					{
						Type: String,
						Id: "name",
					},
					{
						Type: String,
						Id: "role",
					},
				},

				Callback: func(cc CommandCtx, m *discord.Message, am ArgMap) error {
					if am["name"] != name {
						return fmt.Errorf(`name is incorrect, expected "%s", got "%s".`, name, am["name"])
					}
					if am["role"] != role {
						return fmt.Errorf(`role is incorrect, expected "%s", got "%s".`, name, am["role"])
					}

					return nil
				},
			},
		},
	}

	err := handler.Handle(prefix, &discord.Message{
		Content: prefix + `test "Jack C" President`,
	})

	if err != nil {
		t.Error(err)
	}
}