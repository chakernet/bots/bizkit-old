/*
	Command Handler Struct(s)
    Copyright (C) 2021 Jack C <jack@chaker.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package handler

import (
	"errors"
	"regexp"
	"strings"

	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/session"
	"github.com/google/shlex"
	"github.com/streadway/amqp"
	"gitlab.com/chakernet/bots/bizkit/common/redis"
)

type CommandHandler struct {
	Channel *amqp.Channel
	Discord *session.Session
	Redis   *redis.Redis

	Commands []Command
}

type CommandCtx struct {
	Discord *session.Session
	Redis   *redis.Redis
	Command Command
}

type ParsedData struct {
	Prefix string
	Alias string
	Content string
}

type Arg struct {
	Id string
	Type string
}

var (
	String = "string"
	Member = "member"
	User = "user"
)

type Command struct {
	Id string
	Aliases []string
	Args []Arg

	Callback func(CommandCtx, *discord.Message, ArgMap) error
}

type ArgMap = map[string]interface{}

func parseBefore(p string, c string) ParsedData {
	parsed := ParsedData {}

	parsed.Prefix = p
	parsed.Alias = strings.Split(strings.Split(c, p)[1], " ")[0]
	parsed.Content = strings.Join(strings.Split(c + " ", " ")[1:], " ")
	
	return parsed
}

func parseArgs(args []Arg, p ParsedData, m *discord.Message, ctx CommandCtx) (ArgMap, error) {
	argM := make(ArgMap)

	shlexed, err := shlex.Split(p.Content)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(shlexed); i++  {
		arg, parsed := args[i], shlexed[i]

		argM[arg.Id] = nil

		switch arg.Type {
			case String:
				argM[arg.Id] = parsed
				break
			case Member:
				// Check if the id is a role
				if strings.HasPrefix(parsed, "<@&") {
					break
				}

				r := regexp.MustCompile("[0-9]+")

				uIdStr := r.FindAllString(parsed, -1)[0]

				uIdS, err := discord.ParseSnowflake(uIdStr)

				if err != nil {
					break
				}

				uId := discord.UserID(uIdS)

				member, err := ctx.Redis.GetMember(m.GuildID, uId)

				if err != nil || member == nil {
					member, err = ctx.Discord.Member(m.GuildID, uId)

					if err != nil || member == nil {
						break
					}
				}

				argM[arg.Id] = member
				break
		}
	}

	return argM, nil
}

func (h *CommandHandler) Handle(prefix string, m *discord.Message) error {
	if !strings.HasPrefix(strings.ToLower(m.Content), strings.ToLower(prefix)) {
		return nil
	}

	parsed := parseBefore(prefix, m.Content)

	foundCommand := make(chan Command)

	go func() {
		for _, cmd := range h.Commands {
			var found bool
			for _, alias := range cmd.Aliases {
				if alias == parsed.Alias {
					found = true
					break
				}
			}

			if found {
				foundCommand <- cmd
				return
			}
		}
	}()

	command := <-foundCommand

	if foundCommand == nil {
		// No command found
		return errors.New("invalid command")
	}

	// Parse args
	var parsedArgs ArgMap = nil
	var err error = nil

	ctx := CommandCtx{
		Command: command,
		Discord: h.Discord,
		Redis: h.Redis,
	}

	if command.Args != nil {
		parsedArgs, err = parseArgs(command.Args, parsed, m, ctx)
	}

	if err != nil {
		// Send message notifying them of error
		return err
	}

	err = command.Callback(ctx, m, parsedArgs)

	if err != nil {
		// Send message notifying them of error
		return err
	}

	return nil
}