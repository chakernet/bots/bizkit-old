module gitlab.com/chakernet/bots/bizkit/gateway

go 1.17

require (
	gitlab.com/chakernet/bots/bizkit/common v0.0.0-00010101000000-000000000000
	github.com/diamondburned/arikawa/v3 v3.0.0-rc.2
	github.com/getsentry/sentry-go v0.11.0
	github.com/joho/godotenv v1.4.0
	github.com/streadway/amqp v1.0.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/go-redis/redis/v8 v8.11.4 // indirect
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510 // indirect
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
)

replace gitlab.com/chakernet/bots/bizkit/common => ../common
