/*
	Ping Command
    Copyright (C) 2021 Jack C <jack@chaker.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package commands

import (
	"fmt"

	"github.com/diamondburned/arikawa/v3/discord"
	"gitlab.com/chakernet/bots/bizkit/common/handler"
)

func ping(ctx handler.CommandCtx, m *discord.Message, args handler.ArgMap) error {
    ctx.Discord.SendMessage(m.ChannelID, "Pong!")

	return nil
}

func hello(ctx handler.CommandCtx, m *discord.Message, args handler.ArgMap) error {
    ctx.Discord.SendMessage(m.ChannelID, fmt.Sprintf("Hello, %s", args["name"]))

	return nil
}